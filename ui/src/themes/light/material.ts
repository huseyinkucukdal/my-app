import blue from "@material-ui/core/colors/blue";

export default {
  palette: {
    type: "light",
    primary: blue,
  }
};
