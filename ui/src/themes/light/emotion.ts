export default {
  breakpoints: ['52em'],
  colors: {
    bg1: "#ebf6ff",
    bg2: "#fffff4",
    color1: "#07c",
  },
  fonts: {
    body: "Roboto, sans-serif",
    heading: "inherit",
    monospace: "Menlo, monospace",
  },
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],
  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700,
  },
  lineHeights: {
    body: 1.5,
    heading: 1.25,
  },
  space: [0, 5, 10, 15, 20, 25, 30, 35, 40]
};
