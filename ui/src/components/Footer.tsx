import React, { FC } from "react";
import { Flex } from "components/layout";
import { Title } from "components/typography";
import { useTranslation } from "utils";

type Props = {};

const Footer: FC<Props> = () => {
  const { t } = useTranslation();
  return (
    <Flex alignItems="center" justifyContent="center" p={4} bg="bg1">
      <Title color="color1">{t("footer")}</Title>
    </Flex>
  );
};

export default Footer;
