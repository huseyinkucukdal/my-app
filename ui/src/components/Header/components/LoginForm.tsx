import React, { FC, useState } from "react";
import { Button, TextField } from "components/material";
import { Box } from "components/layout";
import { Title } from "components/typography";
import { Modal } from "components/base";
import { useTranslation, makeStyles, useDispatch } from "utils";

type Props = {};

type FormData = {
  name?: string;
  email?: string;
  password?: string;
};

const useStyles: any = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const LoginForm: FC<Props> = () => {
  const { t } = useTranslation();
  const [formData, setFormData] = useState<FormData>({});
  const [isModalVisible, setModalVisibility] = useState<boolean>(false);
  const classes = useStyles();
  const dispatch = useDispatch();
  return (
    <Box>
      <Button
        variant="contained"
        color="primary"
        onClick={() => setModalVisibility(true)}
      >
        {t("login")}
      </Button>
      <Modal
        onClose={() => setModalVisibility(false)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={isModalVisible}
      >
        <div>
          <Box>
            <Title>{t("loginForm")}</Title>
            <form
              onSubmit={(ev) => {
                dispatch({ type: "LOGIN", user: formData });
                ev.preventDefault();
              }}
            >
              <TextField
                onChange={(e) =>
                  setFormData({ ...formData, name: e.target.value })
                }
                variant="outlined"
                margin="normal"
                required
                fullWidth
                label={t("name")}
                name="name"
                autoFocus
              />
              <TextField
                onChange={(e) =>
                  setFormData({ ...formData, email: e.target.value })
                }
                variant="outlined"
                margin="normal"
                required
                fullWidth
                label={t("emailAddress")}
                name="email"
                autoFocus
              />
              <TextField
                onChange={(e) =>
                  setFormData({ ...formData, password: e.target.value })
                }
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label={t("password")}
                type="password"
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                {t("submit")}
              </Button>
            </form>
          </Box>
        </div>
      </Modal>
    </Box>
  );
};

export default LoginForm;
