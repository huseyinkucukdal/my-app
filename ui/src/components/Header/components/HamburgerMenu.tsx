import React, { FC, useState } from "react";
import { Hamburger } from "components/Icons/Hamburger";
import { Popover } from "components/material";
import { Box } from "components/layout";
import HeaderActions from "components/Header/components/HeaderActions";

type Props = {};

const HamburgerMenu: FC<Props> = () => {
  const [anchorEl, setAnchorEl] = useState<null | EventTarget>(null);
  return (
    <>
      <Hamburger
        style={{ cursor: "pointer" }}
        fontSize={4}
        color="color1"
        onClick={(e) => setAnchorEl(e.currentTarget)}
      />
      <Popover
        open={Boolean(anchorEl)}
        // @ts-ignore
        anchorEl={anchorEl}
        onClose={() => setAnchorEl(null)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Box p={3}>
          <HeaderActions />
        </Box>
      </Popover>
    </>
  );
};

export default HamburgerMenu;
