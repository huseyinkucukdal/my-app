import React, { FC } from "react";
import { Flex } from "components/layout";
import { Body } from "components/typography/Body";
import { Link, useDispatch, useTranslation, useSelector, _ } from "utils";
import LoginForm from "./LoginForm";
import UserActionDropdown from "./UserActionDropdown";

type Props = {};
type User = {
  name: string;
  email: string;
  password: string;
};

const HeaderActions: FC<Props> = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const user: User = useSelector((state) => state.userReducer.user);
  return (
    <Flex alignItems="center">
      <Body
        cursor="pointer"
        onClick={() => i18n.changeLanguage("tr")}
        mr={3}
        ml={4}
        color="color1"
      >
        Tr
      </Body>
      <Body
        cursor="pointer"
        onClick={() => i18n.changeLanguage("en")}
        mr={3}
        color="color1"
      >
        En
      </Body>
      <Body
        color="color1"
        cursor="pointer"
        mr={3}
        onClick={() => {
          dispatch({ type: "SET_THEME", theme: "dark" });
        }}
      >
        {t("dark")}
      </Body>
      <Body
        mr={4}
        color="color1"
        cursor="pointer"
        onClick={() => {
          dispatch({ type: "SET_THEME", theme: "light" });
        }}
      >
        {t("light")}
      </Body>
      <Link to="/">
        <Body mr={3} color="color1">
          {t("home")}
        </Body>
      </Link>
      <Link to="/contact_us">
        <Body color="color1" mr={3} cursor="pointer" >
          {t("contactus")}
        </Body>
      </Link>
      {_.isEmpty(user) && <LoginForm />}
      {!_.isEmpty(user) && <UserActionDropdown />}
    </Flex>
  );
};

export default HeaderActions;
