import React, { FC, useState } from "react";
import { useDispatch, useSelector, useTranslation } from "utils";
import { Box } from "components/layout";
import { Body } from "components/typography";
import { Menu, MenuItem } from "components/material";

type Props = {};
type User = {
  name: string;
  email: string;
  password: string;
};

const UserActionDropdown: FC<Props> = () => {
  const user: User = useSelector((state) => state.userReducer.user);
  const [anchorEl, setAnchorEl] = useState<null | EventTarget>(null);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  return (
    <Box>
      <Body
        onClick={(e) => setAnchorEl(e.currentTarget)}
        cursor="pointer"
        textTransform="capitalize"
      >
        {user.name}
      </Body>
      <Menu
        // @ts-ignore
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
      >
        <MenuItem onClick={() => setAnchorEl(null)}>
          <Body>{user.email}</Body>
        </MenuItem>
        <MenuItem
          onClick={() => {
            setAnchorEl(null);
            dispatch({ type: "LOGOUT" });
          }}
        >
          <Body>{t("logout")}</Body>
        </MenuItem>
      </Menu>
    </Box>
  );
};

export default UserActionDropdown;
