import React, { FC } from "react";
import { useLocation, _, useTranslation } from "utils";
import { Flex, Box } from "components/layout";
import { Title } from "components/typography";
import { Bitcoin } from "components/Icons";
import HeaderActions from "./components/HeaderActions";
import HamburgerMenu from "./components/HamburgerMenu";

type Props = {};

const Header: FC<Props> = () => {
  const { t } = useTranslation();
  const { pathname } = useLocation();
  const chars: { [key: string]: string } = { "/": "", _: "" };
  const replacedPathname: string = pathname.replace(/\/|_/g, (m) => chars[m]);
  const pageName: string = _.isEmpty(replacedPathname)
    ? "Bitcoin"
    : replacedPathname;
  return (
    <Flex
      p={2}
      bg="bg1"
      alignItems="center"
      justifyContent="space-between"
      width={1}
      height={56}
    >
      <Flex alignItems="center">
        <Bitcoin fontSize={4} color="color1" />
        <Title ml={2} color="color1">
          {t(pageName)}
        </Title>
      </Flex>
      <Flex display={["Flex", "none"]} alignItems="center">
        <HamburgerMenu />
      </Flex>
      <Box display={["none", "block"]}>
        <HeaderActions />
      </Box>
    </Flex>
  );
};

export default Header;
