import React, { FC, useState } from "react";
import { ModalProps, Modal as MaterialModal } from "@material-ui/core";
import { makeStyles } from "utils";

const getModalStyle = (): object => {
  return {
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  };
};

const useStyles: () => any = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    maxWidth: 400,
    minWidth: 250,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export const Modal: FC<ModalProps> = ({ children, ...others }) => {
  const classes: any = useStyles();
  const [modalStyle] = useState<object>(getModalStyle);

  return (
    <MaterialModal {...others}>
      <div style={modalStyle} className={classes.paper}>
        {children}
      </div>
    </MaterialModal>
  );
};

export default Modal;
