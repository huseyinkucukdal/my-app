import React from "react";
import { styled, color, fontSize, layout, space } from "utils";
import { FaAlignJustify } from "react-icons/fa";

export const Hamburger = styled(FaAlignJustify)`
  ${color} ${fontSize} ${layout} ${space}
`;

export default Hamburger;
