import React from "react";
import { styled, color, fontSize, layout, space } from "utils";
import { FaBitcoin } from "react-icons/fa";

export const Bitcoin = styled(FaBitcoin)`
  ${color} ${fontSize} ${layout} ${space}
`;

export default Bitcoin;
