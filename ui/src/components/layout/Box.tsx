import React, { FC } from "react";
import { Box as RBox, BoxProps } from "rebass";
import { useTheme } from "utils";

interface Props extends BoxProps {
  cursor?: string;
  textTransform?: string;
  style?: any;
}

type Styles = {
  cursor?: string;
  textTransform?: string;
  style?: any;
};

export const Box: FC<Props> = ({ cursor, textTransform, style, ...others }) => {
  const theme = useTheme();
  const styles: Styles = { cursor, textTransform, ...style };
  return (
    // @ts-ignore
    <RBox theme={theme} style={styles} {...others} />
  );
};

export default Box;
