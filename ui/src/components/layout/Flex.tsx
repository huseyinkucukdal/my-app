import React, { FC } from "react";
import { Flex as RFlex, FlexProps } from "rebass";
import { useTheme } from "utils";

interface Props extends FlexProps {
  cursor?: string;
  textTransform?: string;
  style?: any;
}

type Styles = {
  cursor?: string;
  textTransform?: string;
  style?: any;
};

export const Flex: FC<Props> = ({
  cursor,
  textTransform,
  style,
  ...others
}) => {
  const theme = useTheme();
  const styles: Styles = { cursor, textTransform, ...style };
  return (
    // @ts-ignore
    <RFlex theme={theme} style={styles} {...others} />
  );
};

export default Flex;
