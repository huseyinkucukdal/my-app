import React, { FC } from "react";

type Props = {};

const Loader: FC<Props> = () => {
  const theme: string = localStorage.getItem("bitcoinTheme")
    ? localStorage.getItem("bitcoinTheme")
    : "light";
  return (
    <div className={`loading_container ${theme}_bg`}>
      <div className="loading_flex">
        <div className={`loader ${theme}_loader`} />
      </div>
      <div className={`load_text ${theme}_load_text`}>Loading...</div>
    </div>
  );
};

export default Loader;
