import React, { FC } from "react";
import { Text, TextProps } from "rebass";
import { useTheme } from "utils";

interface Props extends TextProps {
  cursor?: string;
  textTransform?: string;
  style?: any;
}

type Styles = {
  cursor?: string;
  textTransform?: string;
  style?: any;
};

export const Body: FC<Props> = ({
  cursor,
  textTransform,
  style,
  ...others
}) => {
  const theme = useTheme();
  const styles: Styles = { cursor, textTransform, ...style };
  return (
    // @ts-ignore
    <Text color="color1" theme={theme} style={styles} {...others} />
  );
};

export default Body;
