import React, { FC } from "react";
import { Heading, HeadingProps } from "rebass";
import { useTheme } from "utils";

interface Props extends HeadingProps {
  cursor?: string;
  textTransform?: string;
  style?: any;
}

type Styles = {
  cursor?: string;
  textTransform?: string;
  style?: any;
};

export const Title: FC<Props> = ({
  cursor,
  textTransform,
  style,
  ...others
}) => {
  const theme = useTheme();
  const styles: Styles = { cursor, textTransform, ...style };
  return (
    // @ts-ignore
    <Heading color="color1" theme={theme} style={styles} {...others} />
  );
};

export default Title;
