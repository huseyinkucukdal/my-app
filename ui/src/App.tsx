import React, { FC } from "react";
import { ThemeProvider as EmotionThemeProvider } from "@emotion/react";
import { ThemeProvider as MaterialThemeProvider } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import emotionLightTheme from "themes/light/emotion";
import emotionDarkTheme from "themes/dark/emotion";
import materialLightTheme from "themes/light/material";
import materialDarkTheme from "themes/dark/material";
import Header from "components/Header";
import Footer from "components/Footer";
import { Box } from "components/layout";
import routes from "./routes";
import { _, useSelector } from "utils";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

type Props = {};

enum ThemesNames {
  light = "light",
  dark = "dark",
}

const emotionThemes: { [themeName: string]: object } = {
  light: emotionLightTheme,
  dark: emotionDarkTheme,
};

const materialThemes: { [themeName: string]: object } = {
  light: materialLightTheme,
  dark: materialDarkTheme,
};

const App: FC<Props> = () => {
  const selectedTheme: ThemesNames = useSelector(
    (state) => state.themeReducer.theme
  );
  const selectedMaterialTheme = createMuiTheme(materialThemes[selectedTheme]);
  return (
    <EmotionThemeProvider theme={emotionThemes[selectedTheme]}>
      <MaterialThemeProvider theme={selectedMaterialTheme}>
        <Router>
          <Header />
          <Box minHeight="calc(100vh - 126px)" bg="bg2">
            <Switch>
              {_.map(routes, (V, p) => (
                <Route key={p} path={p} exact={p === "/"} component={V} />
              ))}
            </Switch>
          </Box>
          <Footer />
        </Router>
      </MaterialThemeProvider>
    </EmotionThemeProvider>
  );
};

export default App;
