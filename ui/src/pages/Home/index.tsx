import React, { FC } from "react";
import { Box, Flex } from "components/layout";
import { Body, Title } from "components/typography";
import { useTranslation } from "utils";

type Props = {};

const HomePage: FC<Props> = () => {
  const { t } = useTranslation();
  return (
    <Box p={5}>
      <Flex justifyContent="center" mb={4}>
        <Title color="color1">{t("homePage")}</Title>
      </Flex>
      <Flex justifyContent="center">
        <Body color="color1">{t("dummyText")}</Body>
      </Flex>
    </Box>
  );
};

export default HomePage;
