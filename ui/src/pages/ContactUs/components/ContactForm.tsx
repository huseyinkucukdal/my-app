import React, { FC, useState, useEffect } from "react";
import {
  Button,
  TextField,
  MenuItem,
  Autocomplete,
  Alert,
} from "components/material";
import { Box } from "components/layout";
import { Body } from "components/typography";
import { useTranslation, makeStyles, useSelector, _ } from "utils";

type Props = {};

const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

type User = {
  name: string;
  email: string;
};

const ContactForm: FC<Props> = () => {
  const { t } = useTranslation();
  const [isAlertVisible, setAlertVisibility] = useState<boolean>(false);
  const [name, setName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [phone, setPhone] = useState<string>("");
  const [countryCode, setCountryCode] = useState<string>("");
  const [text, setText] = useState<string>("");
  const user: User = useSelector((state) => state.userReducer.user);
  useEffect(() => {
    setName(_.get(user, "name", ""));
    setEmail(_.get(user, "email", ""));
  }, [user]);
  const classes = useStyles();
  const onSubmit = () => {
    console.log({ name, email, phone, countryCode, text });
    setAlertVisibility(true);
  };
  return (
    <Box maxWidth={500}>
      {isAlertVisible && (
        <Alert
          action={
            <Body
              onClick={() => setAlertVisibility(false)}
              color="color1"
              cursor="pointer"
            >
              {t("close")}
            </Body>
          }
        >
          <Body color="color1">{t("checkConsole")}</Body>
        </Alert>
      )}
      <form
        onSubmit={(ev) => {
          ev.preventDefault();
          onSubmit();
        }}
      >
        <TextField
          onChange={(e) => setName(e.target.value)}
          variant="outlined"
          value={name}
          margin="normal"
          required
          fullWidth
          label={t("name")}
          name="name"
          autoFocus
        />
        <TextField
          onChange={(e) => setEmail(e.target.value)}
          value={email}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          label={t("emailAddress")}
          name="email"
          autoFocus
        />
        <TextField
          onChange={(e) => setPhone(e.target.value)}
          value={phone}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="phone"
          label={t("phone")}
          type="number"
        />
        <Autocomplete
          options={countryCodes}
          getOptionLabel={(option) => option.name}
          onChange={(e, v) => {
            // @ts-ignore
            setCountryCode(_.get(v, "id", ""));
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              label={t("countryCode")}
              value={countryCode}
              variant="outlined"
              name="countryCode"
              required
              margin="normal"
              fullWidth
            >
              <MenuItem value="TR">Turkey</MenuItem>
              <MenuItem value="US">USA</MenuItem>
              <MenuItem value="GB">Germany</MenuItem>
              <MenuItem value="DE">Sweden</MenuItem>
              <MenuItem value="SE">Kenya</MenuItem>
              <MenuItem value="BR">Brazil</MenuItem>
              <MenuItem value="ZW">Zimbabwe</MenuItem>
            </TextField>
          )}
        />
        <TextField
          onChange={(e) => setText(e.target.value)}
          value={text}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="text"
          multiline
          rows={4}
          label={t("text")}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          {t("send")}
        </Button>
      </form>
    </Box>
  );
};

const countryCodes: { id: string; name: string }[] = [
  { id: "TR", name: "Turkey" },
  { id: "US", name: "United States of America" },
  { id: "GB", name: "United Kingdom" },
  { id: "DE", name: "Germany" },
  { id: "SE", name: "Sweden" },
  { id: "KE", name: "Kenya" },
  { id: "BR", name: "Brazil" },
  { id: "ZW", name: "Zimbabwe" },
];
export default ContactForm;
