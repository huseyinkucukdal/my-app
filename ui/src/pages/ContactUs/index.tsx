import React, { FC } from "react";
import { Box, Flex } from "components/layout";
import { Title } from "components/typography";
import { useTranslation } from "utils";
import ContactForm from "./components/ContactForm";

type Props = {};

const ContactPage: FC<Props> = () => {
  const { t } = useTranslation();
  return (
    <Box p={5}>
      <Flex justifyContent="center" mb={4}>
        <Title color="color1">{t("contactUsPage")}</Title>
      </Flex>
      <Flex justifyContent="center">
        <ContactForm />
      </Flex>
    </Box>
  );
};

export default ContactPage;
