export const setTheme = (state: object, action: { theme: string }): object => {
  localStorage.setItem("bitcoinTheme", action.theme);
  return { ...state, theme: action.theme };
};
