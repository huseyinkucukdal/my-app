export const loginActions = (state: object, action: { user: object }): object => {
  return { ...state, user: action.user };
};
