export const logoutAction = (state: object): object => {
  return { ...state, user: {} };
};
