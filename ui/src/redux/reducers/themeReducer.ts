import { setTheme } from "@redux/actions/themeAction";
import { SET_THEME } from "@redux/actions/actionTypes";

const theme = localStorage.getItem("bitcoinTheme")
  ? localStorage.getItem("bitcoinTheme")
  : "light";
const initialState = { theme };

const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_THEME: {
      return setTheme(state, action);
    }
    default: {
      return state;
    }
  }
};

export default themeReducer;
