import { loginActions } from "@redux/actions/loginActions";
import { logoutAction } from "@redux/actions/logoutAction";
import { LOGIN, LOGOUT } from "@redux/actions/actionTypes";

const initialState = { user: {} };

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN: {
      return loginActions(state, action);
    }
    case LOGOUT: {
      return logoutAction(state);
    }
    default: {
      return state;
    }
  }
};

export default userReducer;
