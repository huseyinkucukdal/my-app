import Home from "pages/Home";
import ContactUs from "pages/ContactUs";

export default {
  "/": Home,
  "/contact_us": ContactUs
}
