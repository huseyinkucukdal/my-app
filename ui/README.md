## GET STARTED

tested node version is 10

### `yarn`
installs packages

### `yarn start`
runs project on port 3000

### `yarn test`
runs tests

### `yarn build`
builds project

## 
&#9989; theme support

&#9989; localization support

&#9989; responsive design support

## 
- react
- typescript
- webpack
- redux
- jest
- material ui

## TODO

### ui

[ ] add Enzyme

[ ] complete unit tests

[ ] add Cpypress

[ ] end to end tests

[ ] add Apollo client

[ ] create user form

[ ] use graphql apis for create user, login, logout, fetch user, update user theme, contact request

### server

[ ] create server folder near ui folder

[ ] set python env

[ ] add flask

[ ] jwt auth

[ ] graphine

[ ] mongo db

[ ] mongo engine

[ ] user and contact request data models

[ ] apis for create user, login, logout, fetch user, update user theme, contact request

[ ] server test mode for end to end tests

### devOps

[ ] add docker

[ ] commit message control / block








